import React from 'react';
import classes from './Order.css';

const Order = (props) => {
    const ingredients = [];
    for (let ingredientsName in props.ingredients) {
        ingredients.push({
            name: ingredientsName,
            amount: props.ingredients[ingredientsName]
        })
    }
    let ingredient = ingredients.map(i => {
        return (<span className={classes.IngBox}>{i.name} ({i.amount})</span>)      
    })
    return (
        
        <div className={classes.Order}>
            <p>Ingredients : {ingredient} </p>
            <p>Price : USD {Number.parseFloat(props.price).toFixed(2)} </p>
        </div>
    )
}

export default Order;