import React from 'react';
import classes from './BuildControl.css';

const BuildControl = (props) => {
    return (
        <div className={classes.BuildControl}>
            <label className={classes.label}>{props.label}</label>
            <button className={classes.Less} onClick={props.less} disabled={props.disabled}>LESS</button>
            <button className={classes.More} onClick={props.more}>MORE</button>
        </div>
    )
}
export default BuildControl;