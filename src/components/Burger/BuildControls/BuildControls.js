import React from 'react';
import BuildControl from './BuildControl/BuildControl';
import classes from './BuildControls.css'

const controls = [
    { label: 'Salad', type: 'Salad'},
    { label: 'Bacon', type: 'Bacon'},
    { label: 'Cheese', type: 'Cheese'},
    { label: 'Meat', type: 'Meat'}
]

const BuildControls = (props) => {
    return(
        <div className={classes.BuildControls}>
            <p>{props.totalPrice.toFixed(2)} $</p>
            {controls.map(ctrl => (
                <BuildControl key={ctrl.label} label={ctrl.label} more={() => {props.more(ctrl.type)}} less={() => {props.less(ctrl.type)}} disabled={props.disabled[ctrl.type]}/>
            ))}
            <button className={classes.OrderButton} disabled={!props.purchasable} onClick={props.orderPlaced}>{props.isAuth ? 'ORDER NOW': 'SIGNUP'}</button>
        </div>
    )
}

export default BuildControls;