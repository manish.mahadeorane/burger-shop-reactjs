import React from 'react';
import Aux from '../../../hoc/auxiliary';
import Button from '../../UI/Button/Button';

const BurgerSummary = (props) => {
    const ingredientSummary = Object.keys(props.ingredients)
                                .map(igKey => {
                                    return (<li key={igKey}>
                                            <span> {igKey} :</span> {props.ingredients[igKey]}
                                        </li>);
                                });
    return (
        <Aux>
            <h3>Your Order</h3>
            <p>Your order contains following ingredients :</p>
            <ul>
                {ingredientSummary}
            </ul>
            <p><strong>Total Price : {props.total.toFixed(2)}$</strong></p>
            <p>Do you want to continue?</p>
            <Button btnType={'Danger'} clicked={props.purchaseCancelled}>Cancel</Button>
            <Button btnType={'Success'} clicked={props.purchaseContinued}>Continue</Button>
        </Aux>        
    )
}

export default BurgerSummary