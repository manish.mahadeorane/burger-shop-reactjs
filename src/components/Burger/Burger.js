import React from 'react';
import classes from './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const Burger = (props) => {
    let transformedIngredient = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_,i) => {
                return <BurgerIngredient type={igKey} key={igKey + i} />;
            })
        })
        .reduce((arr,el) => {
            console.log(arr);
            return arr.concat(el);
        },[])

    if(transformedIngredient.length === 0){
        transformedIngredient = <p>Please select ingredients from list</p>
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="BreadTop"/>
            {transformedIngredient}
            <BurgerIngredient type="BreadBottom"/>
        </div>
    );
}

export default Burger;