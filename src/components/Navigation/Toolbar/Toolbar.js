import React from 'react';
import classes from './Toolbar.css';
import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo';

const Toolbar = (props) => {
    return (
        <div className={classes.Toolbar}>
            <div onClick={props.clicked} className={classes.DrawerToggle}>
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div className={classes.Logo}>
                <Logo />
            </div>
            <nav className={classes.DesktopOnly}>
                <NavigationItems isAuth={props.isAuth}/>
            </nav>
        </div>
    )
}

export default Toolbar;