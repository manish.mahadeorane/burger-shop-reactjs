import React, { Component} from 'react';
import Aux from '../auxiliary';
import Model from '../../components/UI/Model/Model';

const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        state = {
            error: null
        }
        componentWillMount() {
            axios.interceptors.request.use(req => {
                this.setState({
                    error:null
                })
                return req;
            })
            axios.interceptors.response.use(null, err => {
                this.setState({
                    error: err
                })
            })
        }

        errorConfirmedHandler = () => {
            this.setState({error: null})
        }
        render () {
            return (
                <Aux>
                    <Model show={this.state.error} modelClosed={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Model>
                    <WrappedComponent {...this.props}/>
                </Aux>
            )
        }
    }
}

export default withErrorHandler;