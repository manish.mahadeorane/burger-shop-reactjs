import * as actions from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type: actions.AUTH_START
    }
}

export const authSuccess = (userId, token) => {
    return {
        type: actions.AUTH_SUCCESS,
        token: token,
        userId: userId
    }
}

export const authFail = (error) => {
    return {
        type: actions.AUTH_FAIL,
        error: error
    }
}

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime*1000)
    }
}

export const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('expirationTime');
    localStorage.removeItem('userId');
    return {
        type: actions.AUTH_LOGOUT
    };
}

export const setAuthRedirectPath = (path) => {
    return {
        type: actions.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}

export const auth = (username, password, isSignUp) => {
    return dispatch => {
        dispatch(authStart());

        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyAbiwIX_s4H1zGesQ-gm1sOszq3H-fVp64';

        if(!isSignUp) {
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAbiwIX_s4H1zGesQ-gm1sOszq3H-fVp64';
        }

        let authData = {
            email: username,
            password: password,
            returnSecureToken: true
        }

        axios.post(url, authData)
            .then(response => {
                console.log(response.data);
                const expirationTime = new Date(new Date().getTime() + response.data.expiresIn * 1000);
                localStorage.setItem('token', response.data.idToken);
                localStorage.setItem('expirationTime', expirationTime);
                localStorage.setItem('userId', response.data.email);
                dispatch(authSuccess(response.data.email, response.data.idToken));
                dispatch(checkAuthTimeout(response.data.expiresIn));
            })
            .catch(error => {
                console.log(error);
                dispatch(authFail(error.response.data.error))
            })

    }
}

export const checkAuthSucess = () => {
    let token = localStorage.getItem('token');
    let expirationTIme = localStorage.getItem('expirationTime');
    let userId =  localStorage.getItem('userId');
    return dispatch => {
        if(!token) {
            dispatch(logout());
        } else {
            const expirataionDate = new Date(expirationTIme);
            if(expirataionDate > new Date()) {
                dispatch(authSuccess(userId, token));
                dispatch(checkAuthTimeout((expirataionDate.getTime() - new Date().getTime()) / 1000));
            }
            else {
                dispatch(logout());
            }
        }

    }
}