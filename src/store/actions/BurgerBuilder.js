import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const addIngredients = (ingName) => {
    return {
        type: actionTypes.ADDINGREDIENTS,
        ingName: ingName
    }
}

export const removeIngredients = (ingName) => {
    return {
        type: actionTypes.REMOVEINGREDIENTS,
        ingName: ingName
    }
}

export const setIngredient = (ingredients) => {
    return {
        type: actionTypes.SET_INGREDIENTS,
        ingredients: ingredients
    }
}

export const FetchIngredientError = () => {
    return {
        type: actionTypes.FETCH_INGREDIENTS_ERROR
    }
}

export const initIngredients = () => {
    return dispatch => {
        axios.get('/Ingredients.json')
            .then(response => {
                dispatch(setIngredient(response.data))
            })
            .catch(err => {
                dispatch(FetchIngredientError())
            })
    }
}