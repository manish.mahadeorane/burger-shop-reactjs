import * as actionType from './actionTypes'
import Axios from '../../axios';

export const purchaseBurgerSuccess = (id, formData) => {
    return {
        type: actionType.PURCHASE_BURGER_SUCCESS,
        orderId : id,
        formData: formData
    }
}

export const purchaseBurgerFail = (error) => {
    return {
        type: actionType.PURCHASE_BURGER_FAILED,
        error: error
    }
}

export const purchaseBurgerStarted = () => {
    return {
        type: actionType.PURCHASE_BURGER_START
    }
}

export const purchaseInit = () => {
    return {
        type: actionType.PURCHASE_INIT
    }
}
export const purchaseBurger = (formData, token) => {
    return dispatch => {
        dispatch(purchaseBurgerStarted());
        Axios.post('/orders.json?auth=' + token, formData)
            .then( respose => {
                console.log(respose.data)
                dispatch(purchaseBurgerSuccess(respose.data.name, formData))
            })
            .then(error => {
                dispatch(purchaseBurgerFail(error))
            })
    }
}

export const fetchOrderSuccess = (orders) => {
    return {
        type: actionType.FETCH_ORDER_SUCCESS,
        orders: orders
    }
}

export const fetchOrderFailed = (error) => {
    return {
        type: actionType.FETCH_ORDER_FAILED,
        error: error
    }
}

export const fetchOrderStarted = () => {
    return {
        type: actionType.FETCH_ORDER_START
    }
}

export const fetchOrder = (token,userId) => {
    return dispatch => {
        dispatch(fetchOrderStarted());
        const queryParams = '?auth=' + token + '&orderBy="userId"&equalTo="' + userId + '"';
        Axios.get('/orders.json'+ queryParams)
            .then(res => {
                const fetchedOrders = [];
                for (let key in res.data) {
                    fetchedOrders.push({
                        ...res.data[key],
                        id: key
                    })
                }
                dispatch(fetchOrderSuccess(fetchedOrders))
            })
            .catch(err => {
                dispatch(fetchOrderFailed(err))
            })
    }
}