import * as actionType from '../actions/actionTypes';

const initialState = {
    orders: [],
    loading: false,
    purchased: false,
}

const reducer = (state = initialState,action) => {
    switch(action.type) {
        case actionType.PURCHASE_INIT : 
            return {
                ...state,
                purchased: false
            }
        case actionType.PURCHASE_BURGER_START : 
            return {
                ...state,
                loading: true
            }
        case actionType.PURCHASE_BURGER_SUCCESS : 
            const newOrder = {
                ...action.formData,
                id: action.orderId,
                formData: action.formData
            }
            return {
                ...state,
                orders: state.orders.concat(newOrder),
                loading:false,
                purchased: true
            }
        case actionType.PURCHASE_BURGER_FAILED : 
            return {
                ...state,
                loading: false 
            }
        case actionType.FETCH_ORDER_SUCCESS : 
            return {
                ...state,
                orders: action.orders,
                loading: false,
            }
        case actionType.FETCH_ORDER_FAILED : 
            return {
                ...state,
                loading: false,
            }
        case actionType.FETCH_ORDER_START : 
            return {
                ...state,
                loading: true
            }
        default :
            return state
    }
}

export default reducer;

