import * as actionType from '../actions/actionTypes';

const initialState = {
    // ingredients: {
    //     Salad: 0,
    //     Bacon: 0,
    //     Cheese: 0,
    //     Meat: 0
    // },
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
}

const INGREDIENT_PRICES = {
    Salad: 0.5,
    Cheese: 0.4,
    Meat: 1.3,
    Bacon: 0.7
}

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionType.ADDINGREDIENTS: 
            return {
                ...state,
                ingredients : {
                    ...state.ingredients,
                    [action.ingName]: state.ingredients[action.ingName] + 1
                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingName],
                building: true
            }
        case actionType.REMOVEINGREDIENTS: 
            return {
                ...state,
                ingredients : {
                    ...state.ingredients,
                    [action.ingName]: state.ingredients[action.ingName] - 1
                },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingName],
                building: true
            }
        case actionType.SET_INGREDIENTS : 
            return {
                ...state,
                ingredients: action.ingredients,
                error: false,
                totalPrice: 4,
                building:false
            }
        case actionType.FETCH_INGREDIENTS_ERROR : 
            return {
                ...state,
                error: true
            }
        default: 
            return state
    }
}

export default reducer;
