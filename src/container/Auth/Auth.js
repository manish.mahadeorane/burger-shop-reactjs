import React , {Component} from 'react';
import { connect } from 'react-redux';
import Input from '../../components/UI/Input/Input';
import Button from '../../components/UI/Button/Button';
import * as action from '../../store/actions/index';
import classes from './Auth.css';
import Spinner from '../../components/UI/Spinner/Spinner';
import { Redirect } from 'react-router-dom';

class Auth extends Component {
    state = {
        controls : {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Enter Mail'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Enter Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            }
        },
        isSignUp: true
    }
    checkValidity(value, rules) {
        let isValid = true;

        if(rules.required) {
           isValid = value.trim() !== '' && isValid;
        }

        if(rules.minLength) {
            isValid = value.length >= rules.minLength && isValid;
        }

        if (rules.minLength) {
           isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    componentDidMount = () => {
        if(!this.props.building && this.props.setAuthRedirectPath !== '/'){
            this.props.onSetAuthRedirectPath();
        } 
    }

    authHandler = (event) => {
        event.preventDefault();
        let username = this.state.controls.email.value;
        let password = this.state.controls.password.value;
        this.props.onAuth(username, password, this.state.isSignUp);
        
    }
    inputChangedHandler = (event, controlName) => {
        let updatedControls = {
            ...this.state.controls,
            [controlName] : {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation ),
                touched: true,
            }
        }
        this.setState({
            controls: updatedControls
        })
        console.log(this.state.controls)
    }
    changeSignuptoSignInHandler = () => {
        // this.setState(prevState => {
        //     return {isSignUp: !prevState.isSignUp}
        // });

        this.setState({
            isSignUp: !this.state.isSignUp
        })
    }
    render() {
        let updatedArray = [];
        for(let key in this.state.controls) {
            updatedArray.push({
                id: key,
                config: this.state.controls[key]
            })
        }
        let controls = updatedArray.map(input => {
            return (
                <Input 
                    key={input.id} 
                    elementType={input.config.elementType} 
                    elementConfig={input.config.elementConfig} 
                    value={input.config.value}
                    changed={(event) => {this.inputChangedHandler(event,input.id)}}
                    inValid={!input.config.valid}
                    isTouched={input.config.touched}
                />
            )
        })

        
        if(this.props.loading) {
            controls = <Spinner />
        }
        return(
            <div className={classes.Auth}>
                {this.props.token ? <Redirect to={this.props.setAuthRedirectPath} /> : null}
                <form onSubmit={(event) => {this.authHandler(event)}}>
                    {controls}
                    <Button btnType='Success'>Submit</Button>
                </form>
                <Button btnType='Danger' clicked={this.changeSignuptoSignInHandler} >Change to {this.state.isSignUp ? 'SIGN IN' : 'SIGN UP'}</Button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        token: state.auth.token,
        userId: state.auth.userId,
        loading: state.auth.loading,
        error: state.auth.error,
        building: state.burgerBuilder.building,
        setAuthRedirectPath: state.auth.setAuthRedirectPath
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth : (username,password,isSignUp) => dispatch(action.auth(username,password,isSignUp)),
        onSetAuthRedirectPath : (path) => dispatch(action.setAuthRedirectPath('/'))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);