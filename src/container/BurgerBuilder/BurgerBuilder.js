import React, { Component } from 'react';
import Aux from '../../hoc/auxiliary';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Model from '../../components/UI/Model/Model';
import BurgerSummary from '../../components/Burger/BurgerSummary/BurgerSummary';
import axios from '../../axios';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import {connect} from 'react-redux';
import * as actionTypes from '../../store/actions/index';



class BurgerBuilder extends Component {
    state = {
        // ingredients : {
        //     Meat: 0,
        //     Salad: 0,
        //     Cheese: 0,
        //     Bacon: 0
        // },
        // ingredients: null,
        // totalPrice : 4,
        // purchasable: false,
        purchasing: false,
        loading: false,
        // error: false
    }
    
    componentDidMount() {
        // axios.get('/Ingredients.json')
        //     .then(response => {
        //         this.setState({
        //             ingredients: response.data
        //         })
        //         this.updatePurchased(response.data)
        //         this.updatePrice(response.data)
        //     })
        //     .catch(err => {
        //         this.setState({
        //             error: true
        //         })
        //     })
        this.props.onInitIngredients();
    }

    updatePurchased = (ingredient) => {
        const sum = Object.keys(ingredient)
            .map(igkey => {
                return ingredient[igkey]; 
            })
            .reduce((sum, el) => {
                return sum + el ;
            },0);
        return sum > 0
    }

    purchaseHadler = () => {
        if(this.props.isAuthenticated) {
            this.setState({
                purchasing: true
            })
        }else{
            this.props.onSetAuthRedirectPath('/checkout');
            this.props.history.push('/auth');
        }
        
    }

    purchaseCancelHadler = () => {
        this.setState({
            purchasing: false
        })
    }

    purchaseContinuedHandler = () => {
        this.props.onInit();
        this.props.history.push('/checkout')
    }

    render(){
        const disabled = {
            ...this.props.ing
        };
        for (let key in disabled) {
            disabled[key] = disabled[key] <= 0
        }
        let orderSummary = null;

        let burger = this.props.error ? <p>Ingredients can't be loaded!</p> : <Spinner />;
        if(this.props.ing) {
            burger = (
                <Aux>
                    <Burger ingredients={this.props.ing}/>
                    <BuildControls 
                        more={this.props.onAddIngredients} 
                        orderPlaced={this.purchaseHadler} 
                        less={this.props.onRemoveIngredients} 
                        disabled={disabled} 
                        purchasable={this.updatePurchased(this.props.ing)} 
                        totalPrice={this.props.price}
                        isAuth={this.props.isAuthenticated}
                    />
                </Aux>
            )
            orderSummary = <BurgerSummary ingredients={this.props.ing} total={this.props.price} purchaseContinued={this.purchaseContinuedHandler} purchaseCancelled={this.purchaseCancelHadler}/>
        }
        if(this.state.loading) {
            orderSummary = <Spinner />
        }

        return(
            <Aux>
                <Model show={this.state.purchasing} modelClosed={this.purchaseCancelHadler}>
                   {orderSummary} 
                </Model>
               {burger}
            </Aux>
        );   
    }
}

const mapStateToProps = state => {
    return {
        ing: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddIngredients: (item) => dispatch(actionTypes.addIngredients(item)),
        onRemoveIngredients: (item) => dispatch(actionTypes.removeIngredients(item)),
        onInitIngredients: () => dispatch(actionTypes.initIngredients()),
        onInit: () => dispatch(actionTypes.purchaseInit()),
        onSetAuthRedirectPath: (path) => dispatch(actionTypes.setAuthRedirectPath(path))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));