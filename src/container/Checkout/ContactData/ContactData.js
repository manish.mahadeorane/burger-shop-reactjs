import React , { Component } from 'react';
import Button from '../../../components/UI/Button/Button';
import classes from './ContactData.css';
import axios from '../../../axios';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Input from '../../../components/UI/Input/Input';
import { connect } from 'react-redux';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler'
import * as actions from '../../../store/actions/index';

class ContactData extends Component{
    state = {
        orderForm: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            street: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter Street'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            potsalCode: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Postal Code'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5
                },
                valid: false,
                touched: false
            },
            country: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Country'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Enter Mail'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            deliveryMethod: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: 'fastest', displayValue: 'Fastest'},
                        {value: 'slowest', displayValue: 'Slowest'}
                    ]
                },
                validation: {},
                value: 'fastest',
                valid: true
            }
        },
        formValid: false,
        loading: false,
    }
    orderHandler = (event) => {
        event.preventDefault();
        
        const formData = {};
        for(let formElementIdentifier in this.state.orderForm ) {
            formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value
        }
        const burgerOrder = {
            ingredients: this.props.ing,
            price: this.props.price,
            customer: formData,
            userId: this.props.userId
        }

        // axios.post('/orders.json', burgerOrder)
        //     .then(response => {
        //         this.setState({
        //             loading: false
        //         })      
        //         this.props.history.push('/');
        //     })
        //     .catch(err => {
        //         this.setState({
        //             loading: false
        //         })
        //     })

        this.props.onOrderBurger(burgerOrder, this.props.token);
    }
    checkValidity(value, rules) {
         let isValid = true;

         if(rules.required) {
            isValid = value.trim() !== '' && isValid;
         }

         if(rules.minLength) {
             isValid = value.length >= rules.minLength && isValid;
         }

         if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

         return isValid;
    }
    inputChangedHandler = (event, inputIdentifier) => {
        console.log(event.target.value);
        const updatedOrderForm = {
            ...this.state.orderForm
        };
        const updatedFormElement = {
            ...updatedOrderForm[inputIdentifier]
        }

        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedOrderForm[inputIdentifier] = updatedFormElement;
        let formValid = true;
        for (let key in updatedOrderForm) {
            formValid = updatedOrderForm[key].valid && formValid;
        }
        console.log(formValid);
        this.setState({
            orderForm: updatedOrderForm,
            formValid: formValid
        })
    }
    render() {
        let form = null;
        let formInputArray = [];
        for (let key in this.state.orderForm) {
            formInputArray.push({
                id: key,
                config: this.state.orderForm[key]
            })
        }
        form  = (
            <form>
                {formInputArray.map(input => {
                    return <Input 
                                key={input.id} 
                                elementType={input.config.elementType} 
                                elementConfig={input.config.elementConfig} 
                                value={input.config.value}
                                changed={(event) => {this.inputChangedHandler(event,input.id)}}
                                inValid={!input.config.valid}
                                isTouched={input.config.touched}
                            />
                })}
                <Button btnType='Success' clicked={this.orderHandler} disabled={!this.state.formValid}>Order</Button>
            </form>
        )
        if(this.props.loading) {
            form = <Spinner />
        }

        
        return (
            <div className={classes.ContactData}>
                <h4>Contact Summary Page</h4>
                {form}
            </div>
        )
    }
}

const mapStateToProps =  state => {
    return {
        ing: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onOrderBurger: (formData, token) => dispatch(actions.purchaseBurger(formData, token))
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(withErrorHandler(ContactData, axios));