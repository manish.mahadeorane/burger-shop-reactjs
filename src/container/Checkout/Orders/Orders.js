import React, { Component } from 'react';
import Order from '../../../components/Order/Order';
import Axios from '../../../axios';
import withErrorHandler from '../../../hoc/withErrorHandler/withErrorHandler';
import Spinner from '../../../components/UI/Spinner/Spinner';
import { connect } from 'react-redux';
import * as action from '../../../store/actions/index';

class Orders extends Component {
   
    componentDidMount () {
        this.props.onFetchOrder(this.props.token, this.props.userId);
    }
    render () {
        let orders = <Spinner />;
        if(this.props.orders) {
            orders = this.props.orders.map(order => {
                return (
                    <Order key={order.id} ingredients={order.ingredients} price={order.price}/>
                )
            })
        }
        return (
            <div>
                {orders}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchOrder: (token, userId) => dispatch(action.fetchOrder(token, userId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Orders, Axios));