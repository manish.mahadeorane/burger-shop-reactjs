import React, { Component } from 'react';
import './App.css';
import Layout from './components/Layout/Layout';
import BurgerBuilder from './container/BurgerBuilder/BurgerBuilder';
import Checkout from './container/Checkout/Checkout';
import { Route, Switch, withRouter } from 'react-router-dom';
import Orders from './container/Checkout/Orders/Orders';
import Auth from './container/Auth/Auth';
import Logout from './container/Auth/Logout/Logout';
import * as action from './store/actions/index';
import { connect } from 'react-redux';

class App extends Component {
  componentDidMount(){
    this.props.onCheckAuthSucess();
  }
  render() {
    return (
        <div className="App">
          <Layout>
            <Switch>
              <Route path='/checkout' component={Checkout} />
              <Route path='/orders' exact component={Orders} />
              <Route path='/auth' exact component={Auth} />
              <Route path='/logout' exact component={Logout} />
              <Route path='/' exact component={BurgerBuilder} />
            </Switch>
          </Layout>
        </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCheckAuthSucess : () => dispatch(action.checkAuthSucess())
  }
}

export default withRouter(connect(null,mapDispatchToProps)(App));
